# Process for creating NFTs on Cardano

These notes are probably 90% complete! Let me know what questions you have, and I'll update with better annotations on suggested use of this doc soon.

## Prepare your metadata
- This helps: https://pool.pm/test/metadata
- When your metadata is ready, save it as a `metadata.json` file - we'll need it

## Get Address
- I like to save mine as the environment variable `SENDER` for easy access

## Get UTxO
`cardano-cli query utxo --mainnet --address $SENDER`

## Set SENDERKEY
I like to set the path to my `payment.skey` as the environment variable `SENDERKEY`

## Get protocol parameters
`cardano-cli query protocol-parameters --mainnet --out-file protocol.json`

## Create Policy
```
mkdir policy

cardano-cli address key-gen \
--verification-key-file policy/policy.vkey \
--signing-key-file policy/policy.skey

touch policy/script.policy
cardano-cli address key-hash --payment-verification-key-file policy/policy.vkey

```

Then put that new hash into `policy.script`, which should now look like this:

```
{
    "scripts": [
        {
            "keyHash": "YOUR_KEYHASH_HERE",
            "type": "sig"
        },
        {
            "slot": DO THE ARITHMETIC and PUT A SLOT NUMBER HERE
            "type": "before"
        },
    "type": "all"
    ]
}
```

## Mint the new asset:
```
cardano-cli transaction policyid --script-file policy/policy.script
```

Now you have a new Policy ID!

Place it in the metadata for your new asset.

## Create the raw transaction
```
cardano-cli transaction build-raw \
--tx-in $TXIN \
--tx-out $SENDER+0+"1 YOUR_POLICY_ID_HERE.TOKEN_NAME_A + 1 YOUR_POLICY_ID_HERE.TOKEN_NAME_B" \
--mint="1 YOUR_POLICY_ID_HERE.TOKEN_NAME_A + 1 YOUR_POLICY_ID_HERE.TOKEN_NAME_B" \
--mint-script-file policy/policy.script \
--metadata-json-file metadata.json \
--invalid-hereafter EXPIRATION_SLOT \
--fee 0 \
--out-file tx.draft
```

## Calculate Fee
```
cardano-cli transaction calculate-min-fee \
--tx-body-file tx.draft \
--tx-in-count 1 \
--tx-out-count 1 \
--witness-count 2 \
--protocol-params-file protocol.json
```

## Do some Arithmetic:
```
CHANGE=$(expr $LOVELACE - $FEE)
echo $CHANGE
```

## Draft Real TX
```
cardano-cli transaction build-raw \
--tx-in $TXIN \
--tx-out $SENDER+$CHANGE+"1 YOUR_POLICY_ID_HERE.TOKEN_NAME_A + 1 YOUR_POLICY_ID_HERE.TOKEN_NAME_B" \
--mint="1 YOUR_POLICY_ID_HERE.TOKEN_NAME_A + 1 YOUR_POLICY_ID_HERE.TOKEN_NAME_B" \
--mint-script-file policy/policy.script \
--metadata-json-file metadata.json \
--invalid-hereafter EXPIRATION_SLOT \
--fee $FEE \
--out-file tx.raw
```

## Sign TX
```
cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--signing-key-file policy/policy.skey \
--tx-body-file tx.raw \
--out-file tx.signed
```

## Submit TX
```
cardano-cli transaction submit \
--tx-file tx.signed \
--mainnet
```


cardano-cli transaction build-raw \
--tx-in $TXIN \
--tx-out $SENDER+0 \
--metadata-json-file eachunsig.json \
--fee 0 \
--out-file txn.draft


cardano-cli transaction calculate-min-fee \
--tx-body-file txn.draft \
--tx-in-count 1 \
--tx-out-count 1 \
--witness-count 1 \
--protocol-params-file protocol.json
